package com.foo.bar.equality.impl;

import java.util.Collection;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.jsoup.nodes.Element;

import com.foo.bar.config.CalculationCoefficients;
import com.foo.bar.equality.EqualityPercentageComparator;

public class JsoupElementEqualityComparator implements EqualityPercentageComparator<Element> {

    private static final int EXACT_MATCH_PERCENTAGE = 100;

    private CalculationCoefficients calculationCoefficients;

    public JsoupElementEqualityComparator(CalculationCoefficients calculationCoefficients) {
        this.calculationCoefficients = calculationCoefficients;
    }

    @Override
    public double compareEqualityPercentage(Element sample, Element comparingElement) {
        double total = 0D;

        total += getIdScore(sample, comparingElement);
        total += getAttributeScore(sample, comparingElement);
        total += getBodyTextScore(sample, comparingElement);
        total += getChildrenScore(sample, comparingElement);

        return total;
    }

    private double getIdScore(Element sample, Element comparingElement) {
        if (Objects.equals(sample.tagName(), comparingElement.tagName())) {
            return calculationCoefficients.getTagNameCoefficient() * EXACT_MATCH_PERCENTAGE;
        } else {
            return 0;
        }
    }

    private double getChildrenScore(Element etalon, Element comparingElement) {
        if (comparingElement.childNodeSize() != 0) {
            return EXACT_MATCH_PERCENTAGE * calculationCoefficients.getChildren()
                    * ((double) etalon.childNodeSize() / comparingElement.childNodeSize());
        } else {
            return 0;
        }
    }

    private double getBodyTextScore(Element sample, Element comparingElement) {
        String longestSequenceOfChar = longestSequenceOfChar(sample.text(), comparingElement.text());
        return EXACT_MATCH_PERCENTAGE * calculationCoefficients.getBodyCoefficient()
                * ((double) longestSequenceOfChar.length() / sample.text().length());
    }

    private double getAttributeScore(Element sample, Element comparingElement) {
        Collection<?> commonAttributes = CollectionUtils.intersection(sample.attributes(),
                comparingElement.attributes());

        return EXACT_MATCH_PERCENTAGE * calculationCoefficients.getAttributeCoeficient()
                * ((double) commonAttributes.size() / sample.attributes().size());
    }

    private String longestSequenceOfChar(String mainString, String comparingString) {
        String longestSubstring = "";
        int longestStringLength = 0;

        for (int i = 0; i < mainString.length(); i++) {
            for (int j = i + 1; j <= mainString.length(); j++) {
                String substring = mainString.substring(i, j);
                if (comparingString.contains(substring) && substring.length() > longestStringLength) {
                    longestStringLength = substring.length();
                    longestSubstring = substring;
                }
            }
        }

        return longestSubstring;
    }

}
