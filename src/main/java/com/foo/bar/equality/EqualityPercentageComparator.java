package com.foo.bar.equality;

public interface EqualityPercentageComparator<T> {

    double compareEqualityPercentage(T firstElement, T secondElement);

}
