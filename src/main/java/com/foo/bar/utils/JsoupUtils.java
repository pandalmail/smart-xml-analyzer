package com.foo.bar.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

import org.jsoup.nodes.Element;

public class JsoupUtils {

    private JsoupUtils() {

    }

    public static String getDomElementPath(Element element) {
        ArrayList<Element> traversingElements = new ArrayList<>(element.parents());
        Collections.reverse(traversingElements);

        return traversingElements.stream().sequential()
                .map(JsoupUtils::getElementLocation)
                .collect(Collectors.joining(" > "));
    }

    private static String getElementLocation(Element element) {
        int siblingIndex = element.elementSiblingIndex();
        return siblingIndex == 0
                ? element.tagName()
                : String.format("%s[%d]", element.tagName(), siblingIndex);
    }

}
