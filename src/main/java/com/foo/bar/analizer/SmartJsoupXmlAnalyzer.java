package com.foo.bar.analizer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.foo.bar.equality.EqualityPercentageComparator;

public class SmartJsoupXmlAnalyzer {

    private Document mainDocument;
    private Document searchingDocument;
    private EqualityPercentageComparator<Element> analyzer;
    private List<BiConsumer<Element, Double>> processingListeners = new ArrayList<>();

    private SmartJsoupXmlAnalyzer(Document mainDocument, Document searchingDocument,
                                  EqualityPercentageComparator<Element> analyzer) {
        this.mainDocument = mainDocument;
        this.searchingDocument = searchingDocument;
        this.analyzer = analyzer;
    }

    public static SmartJsoupXmlAnalyzer initAnalizer(File mainHtml, File searchingHtml,
                                                     EqualityPercentageComparator<Element> analyzer)
            throws IOException {
        Document mainDocument = Jsoup.parse(mainHtml, StandardCharsets.UTF_8.name());
        Document searchingDocument = Jsoup.parse(searchingHtml, StandardCharsets.UTF_8.name());
        return new SmartJsoupXmlAnalyzer(mainDocument, searchingDocument, analyzer);
    }

    public Optional<Element> getResemblingElement(Function<Element, Element> etalonGenerator) {
        Element sampleDoc = etalonGenerator.apply(mainDocument.body());
        if (sampleDoc == null) {
            return Optional.empty();
        }

        return searchingDocument.body().getAllElements().stream()
                .map(searchingDoc -> Pair.of(searchingDoc, analyzer.compareEqualityPercentage(sampleDoc, searchingDoc)))
                .peek(this::fireProcessingEvent)
                .max(Comparator.comparingDouble(Pair::getRight))
                .map(Pair::getLeft);
    }

    public void addProcessingListener(BiConsumer<Element, Double> listener) {
        processingListeners.add(listener);
    }

    private void fireProcessingEvent(Pair<Element, Double> elementAndScore) {
        processingListeners.spliterator().forEachRemaining(elementDoubleBiConsumer -> {
            Element element = elementAndScore.getLeft();
            Double score = elementAndScore.getRight();
            elementDoubleBiConsumer.accept(element, score);
        });
    }

}
