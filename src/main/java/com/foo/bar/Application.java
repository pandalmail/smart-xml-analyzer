package com.foo.bar;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import org.apache.commons.lang3.ObjectUtils;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foo.bar.analizer.SmartJsoupXmlAnalyzer;
import com.foo.bar.config.CalculationCoefficients;
import com.foo.bar.equality.EqualityPercentageComparator;
import com.foo.bar.equality.impl.JsoupElementEqualityComparator;
import com.foo.bar.utils.JsoupUtils;


public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private static final String DEFAULT_SEARCHING_ID = "make-everything-ok-button";

    private static final int EXPECTED_ARRAY_LENGTH = 3;

    private static final int MAIN_FILE_INDEX = 0;
    private static final int SEARCHING_FILE_INDEX = 1;
    private static final int SEARCHING_ID_INDEX = 2;

    public static void main(String[] args) {
        args = Arrays.copyOf(args, EXPECTED_ARRAY_LENGTH);

        boolean argsValid = validateArgs(args);
        if (!argsValid) {
            return;
        }

        File mainHtml = new File(args[MAIN_FILE_INDEX]);
        File searchingHtml = new File(args[SEARCHING_FILE_INDEX]);
        String searchingId = ObjectUtils.defaultIfNull(args[SEARCHING_ID_INDEX], DEFAULT_SEARCHING_ID);

        run(mainHtml, searchingHtml, searchingId);
    }

    private static void run(File mainHtml, File searchingHtml, String searchingId) {
        EqualityPercentageComparator<Element> analyzer =
                new JsoupElementEqualityComparator(CalculationCoefficients.buildDefaults());
        Optional<Element> resemblingElement = Optional.empty();
        try {
            SmartJsoupXmlAnalyzer smartJsoupXmlAnalyzer =
                    SmartJsoupXmlAnalyzer.initAnalizer(mainHtml, searchingHtml, analyzer);
            smartJsoupXmlAnalyzer.addProcessingListener((element, score) ->
                    System.out.printf("Processing element %-100s with score %f%%%n",
                            JsoupUtils.getDomElementPath(element), score));
            resemblingElement =
                    smartJsoupXmlAnalyzer.getResemblingElement(element -> element.getElementById(searchingId));
        } catch (IOException e) {
            System.out.println("Cannot read file located as the first argument");
            LOGGER.debug("IOException when reading input files.", e);
        }

        resemblingElement.ifPresent(element -> System.out.printf("The most resemble element found: %n%s. %nPath: %s",
                element, JsoupUtils.getDomElementPath(element)));
        if (!resemblingElement.isPresent()) {
            System.out.println("Resemble element not found. Check that the selector to compare " +
                    "from input file is correct");
        }
    }

    private static boolean validateArgs(String[] args) {
        if (!ObjectUtils.allNotNull(args[MAIN_FILE_INDEX], args[SEARCHING_FILE_INDEX])) {
            System.out.println("Input and output file required. " +
                    "Usage Pattern java -jar <jar> <Input file> <Comparing input file> [<Searching_Element_Id>]");
            return false;
        }
        return true;
    }

}
