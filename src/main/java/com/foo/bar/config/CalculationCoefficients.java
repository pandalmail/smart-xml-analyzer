package com.foo.bar.config;

import java.io.File;

public class CalculationCoefficients {

    private double idCoefficient = 0.1;
    private double tagNameCoefficient = 0.3;
    private double attributeCoefficient = 0.3;
    private double bodyCoefficient = 0.1;
    private double childrenCoefficient = 0.2;

    private CalculationCoefficients(double idCoefficient,
                                    double tagNameCoefficient,
                                    double attributeCoefficient,
                                    double bodyCoefficient,
                                    double childrenCoefficient) {
        this.idCoefficient = idCoefficient;
        this.tagNameCoefficient = tagNameCoefficient;
        this.attributeCoefficient = attributeCoefficient;
        this.bodyCoefficient = bodyCoefficient;
        this.childrenCoefficient = childrenCoefficient;
    }

    private CalculationCoefficients() {

    }

    public double getIdCoefficient() {
        return idCoefficient;
    }

    public double getTagNameCoefficient() {
        return tagNameCoefficient;
    }

    public double getAttributeCoeficient() {
        return attributeCoefficient;
    }

    public double getBodyCoefficient() {
        return bodyCoefficient;
    }

    public double getChildren() {
        return childrenCoefficient;
    }

    public static CalculationCoefficients buildEqualCoefficients() {
        double equalValue = 0.2D;
        return new CalculationCoefficients(equalValue, equalValue, equalValue, equalValue, equalValue);
    }

    public static CalculationCoefficients buildDefaults() {
        return new CalculationCoefficients();
    }

    public static CalculationCoefficients buildFromProperties(File propertiesFile) {
        throw new UnsupportedOperationException(); // todo just a nice feature to have
    }

}
